package com.ju57man.room.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.ju57man.room.database.car.Car;
import com.ju57man.room.database.car.CarDao;
import com.ju57man.room.database.employee.Employee;
import com.ju57man.room.database.employee.EmployeeDao;

// для кожного entity буде створенно однойменна таблиця
@Database(entities = {Employee.class, Car.class}, version = 1)  // версію потрібно інкрементувати після кожної зміни структури ДБ
public abstract class AppDatabase extends RoomDatabase{  // необхідно описати абстрактні методи для отримання Dao об'єктів,
    public abstract EmployeeDao employeeDao();
    public abstract CarDao carDao();
}
/*
Database объект - это стартовая точка. Его создание выглядит так:

AppDatabase db =  Room.databaseBuilder(getApplicationContext(),
       AppDatabase.class, "database").build();
 */