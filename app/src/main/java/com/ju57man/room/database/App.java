package com.ju57man.room.database;

import android.arch.persistence.room.Room;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class App extends AppCompatActivity {

    public static App instance; // екземпляр

    private AppDatabase database;  // we can get it through getDatabase()

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        instance = this;
        //                                  (context,
        database = Room.databaseBuilder(getApplicationContext(),
                AppDatabase.class, "database")  // class which extended RoomDatabase, name database)
                .allowMainThreadQueries()  // we shouldn't get exception with work in UI thread, but its bad practice
                .build();
    }

    public static App getInstance() {
        return instance;  // return App.this
    }

    public AppDatabase getDatabase() {
        return database;
    }
}
