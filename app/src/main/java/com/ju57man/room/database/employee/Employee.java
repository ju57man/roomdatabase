package com.ju57man.room.database.employee;


import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity  // суть, буття
public class Employee {  // Employee - назва таблиці

    // стовпчики в таблиці
    @PrimaryKey
    public long id;

    public String name;

    public int salary;

    /*
    ми могли б зробити змінні private і додати getters and setters
     */
}
