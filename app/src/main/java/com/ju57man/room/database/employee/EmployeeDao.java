package com.ju57man.room.database.employee;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao  // data access object, шлях, тут будемо описувати методи для роботи с DB
public interface EmployeeDao {

//  запит в DB який дає всіх користувачів
    @Query("SELECT * FROM Employee")  // в AppDatabase ми створюємо таблицю з класу Employee.class з назвою Employee (регістр в SQL не є важливим)
    List<Employee> getAll();  // тип повернення - список співробітників, тому що їх може бути кілька

    @Query("SELECT * FROM Employee WHERE id = :id") // вибираємо співробітника по id
    Employee getById(long id); // даємо id і отримуємо одного співробітника

    @Insert
    void insert(Employee employee);  // вставляємо в DB одного співробітника

    @Update
    void update(Employee employee);  // обновляємо данні вже створенного співробітника

    @Delete
    void delete(Employee employee);  // видаляємо співробітника
}
