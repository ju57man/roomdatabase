package com.ju57man.room;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.ju57man.room.database.App;
import com.ju57man.room.database.AppDatabase;
import com.ju57man.room.database.car.CarDao;
import com.ju57man.room.database.employee.Employee;
import com.ju57man.room.database.employee.EmployeeDao;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        AppDatabase db = App.getInstance().getDatabase();  // getting DB object
        EmployeeDao employeeDao = db.employeeDao();  // getting DAO from DB object
        CarDao carDao = db.carDao();

        //====generate_new_employee====
        Employee employee = new Employee();
        employee.id = 1;
        employee.name = "Jhon";
        employee.salary = 10000;
        //==============================

        //====inserting=to=DB====
        employeeDao.insert(employee);
        //=======================

        //====getting_all_employees====
        List<Employee> employees = employeeDao.getAll();
        //=============================

        //====getting_employee_by_id====
        Employee employee1 = employeeDao.getById(1);
        //==============================

        //====update_data====
        employee.salary = 20000;
        employeeDao.update(employee);  // Room буде шукати по ключовому полю(id)
        //===================

        //====delete_employee====
        employeeDao.delete(employee);  // якщо в employee не заповненно id, то за замовчюванням Room буде шукати співробітника з id = 0
        //=======================
    }

}
